ICI ON AJOUTE UN ETUDIANT
![POST](https://gitlab.com/abdoukader/etudiant-crud/-/raw/main/src/main/java/com/groupeisi/etudiantcrud/capture/POST.png?ref_type=heads)

ICI ON A RECUPERE LES ETUDIANTS
![GET](https://gitlab.com/abdoukader/etudiant-crud/-/raw/main/src/main/java/com/groupeisi/etudiantcrud/capture/GET.png?ref_type=heads)

ICI ON A RECUPERE L'ETUDIANT PAR SON ID
![GET-ID](https://gitlab.com/abdoukader/etudiant-crud/-/raw/main/src/main/java/com/groupeisi/etudiantcrud/capture/GET-ID.png?ref_type=heads)

ICI ON MODIFIE L'ETUDIANT
![PUT](https://gitlab.com/abdoukader/etudiant-crud/-/raw/main/src/main/java/com/groupeisi/etudiantcrud/capture/PUT.png?ref_type=heads)

ICI ON SUPPRIME UN ETUDIANT
![DELETE](https://gitlab.com/abdoukader/etudiant-crud/-/raw/main/src/main/java/com/groupeisi/etudiantcrud/capture/DELETE.png?ref_type=heads)
